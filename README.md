# mini tutorial

Необходимо создать папку в корне проекта с именем 'certs'

В папке 'certs' необходимо создать 2 файла jwt-private.pem , jwt-public.pem

Команды которые помогут:

# Generate an RSA private key, of size 2048
openssl genrsa -out jwt-private.pem 2048
# Extract the public key from the key pair, which can be used in a certificate
openssl rsa -in jwt-private.pem -outform PEM -pubout -out jwt-public.pem


## static user

    username="admin"
    password='admin'
---
    username="sam",
    password='qwerty'
