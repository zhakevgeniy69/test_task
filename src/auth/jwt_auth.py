from fastapi import APIRouter, Depends, HTTPException, status, Form
from fastapi.security.oauth2 import OAuth2PasswordBearer
from src.schemas import TokenInfo
from jwt.exceptions import InvalidTokenError
from src.orm import UserService
from src.auth import utils as auth_utils
from src.schemas import UserSchema


router = APIRouter(prefix='/jwt', tags=["JWT"])
oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='/jwt/login/',
)


async def validate_auth_user(
        username: str = Form(),
        password: str = Form(),
):

    unauth_exc = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='invalid username or password',
    )
    user = await UserService.get_user_by_username(username=username)

    if not user:
        raise unauth_exc

    if not await auth_utils.validate_password(password=password, hashed_password=user.password):
        raise unauth_exc

    return user


@router.post("/login", response_model=TokenInfo)
async def auth_user_issue_jwt(user: UserSchema = Depends(validate_auth_user)):
    jwt_payload = {
        "sub": user.id,
        "username": user.username,
        "email": user.email,
    }
    token = await auth_utils.encode_jwt(payload=jwt_payload)
    return TokenInfo(
        access_token=token,
        token_type='Bearer',
    )


async def get_current_token_payload(
        token: str = Depends(oauth2_scheme),
) -> UserSchema:
    try:
        payload = await auth_utils.decode_jwt(
            token=token,
        )
    except InvalidTokenError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f'invalid token err',
        )
    return payload


async def get_current_auth_user(
        payload: dict = Depends(get_current_token_payload),
) -> UserSchema:
    username: str | None = payload.get("username")
    user = await UserService.get_user_by_username(username=username)
    if user:
        return user

    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='token invalid (user not found)',
    )


async def get_current_active_auth_user(
        user: UserSchema = Depends(get_current_auth_user),
):
    if user.active:
        return user

    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail='user inactive'
    )


@router.get('/users/me')
async def auth_user_check_self_info(
        payload: dict = Depends(get_current_token_payload),
        user: UserSchema = Depends(get_current_active_auth_user),
):
    iat = payload.get('iat')
    return {
        'Username': user.username,
        'Email': user.email,
        'Logged_in_at': iat
    }