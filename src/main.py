import asyncio
import uvicorn
from fastapi import FastAPI
from orm import CreateDB, TestORM
from router import router as api_router
from auth.jwt_auth import router as jwt_router


async def main():
    await CreateDB.create_tables()
    # await TestORM.create_static_posts()
    # await TestORM.create_static_users()
    # await TestORM.create_fictive_votes()


def create_fastapi_app():
    app = FastAPI(title="FastAPI")

    app.include_router(api_router)
    app.include_router(jwt_router)

    return app


app = create_fastapi_app()


if __name__ == '__main__':
    asyncio.run(main())
    uvicorn.run(
        app="main:app",
        reload=True,
        host="0.0.0.0",
        port=8000,
    )

