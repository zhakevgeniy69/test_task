from typing import Annotated, Optional
from sqlalchemy.orm import mapped_column, Mapped, relationship
from database import str_256, Base
from sqlalchemy import text, ForeignKey
import datetime
import enum


intpk = Annotated[int, mapped_column(primary_key=True)]
created_at = Annotated[datetime.datetime, mapped_column(server_default=text("TIMEZONE('utc', now())"))]
updated_at = Annotated[datetime.datetime, mapped_column(
        server_default=text("TIMEZONE('utc', now())"),
        onupdate=datetime.datetime.utcnow,
    )]


class Post(Base):
    __tablename__ = "posts"

    id: Mapped[intpk]
    theme: Mapped[str_256]
    content: Mapped[str]
    author: Mapped[str_256]
    rating: Mapped[int] = mapped_column(default=0)
    created_at: Mapped[created_at]
    updated_at: Mapped[updated_at]

    votes: Mapped['Vote'] = relationship(back_populates='post')


class User(Base):
    __tablename__ = "users"

    id: Mapped[intpk]
    username: Mapped[str]
    password: Mapped[bytes]
    # role: Mapped[str] = mapped_column(default='user')
    active: Mapped[bool] = mapped_column(default=True)
    email: Mapped[Optional[str]]

    votes: Mapped['Vote'] = relationship(back_populates='user')


class VotesType(enum.Enum):
    plus = "plus"
    minus = "minus"
    cancel = "cancel"


class Vote(Base):
    __tablename__ = "votes"
    id: Mapped[intpk]
    user_id: Mapped[int] = mapped_column(ForeignKey('users.id', ondelete='CASCADE'))
    post_id: Mapped[int] = mapped_column(ForeignKey('posts.id', ondelete='CASCADE'))
    vote_type: Mapped[VotesType]
    created_at: Mapped[created_at]

    post: Mapped['Post'] = relationship(back_populates='votes')
    user: Mapped['User'] = relationship(back_populates='votes')

