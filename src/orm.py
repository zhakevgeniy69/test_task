from models import Post, User, Vote, VotesType
from database import async_engine, async_session_factory, Base
from schemas import PostsDTO, UserSchema
from sqlalchemy import select, desc, func, union_all
from src.auth import utils as auth_utils


class TestORM:
    @staticmethod
    async def create_static_users():
        async with async_session_factory() as session:
            user1 = User(
                username="admin",
                password=auth_utils.hash_password('admin'),
                email='admin@gmail.com'
            )
            user2 = User(
                username="sam",
                password=auth_utils.hash_password('qwerty'),
                email='suhov99@gmail.com'
            )
            session.add_all([user1, user2])
            await session.commit()

    @staticmethod
    async def create_static_posts():
        async with async_session_factory() as session:
            post1 = Post(
                theme="top 3 movie",
                content="Побег из Шоушенка, Криминальное чтиво, Молчание ягнят",
                author='NabokovSuper'
            )
            post2 = Post(
                theme="top 3 serial",
                content="Чернобыль, Лучше звоните Солу, Настоящий детектив",
                author='superstas'
            )
            session.add_all([post1, post2])
            await session.commit()

    @staticmethod
    async def create_fictive_votes():
        async with async_session_factory() as session:
            votes_plus = Vote(
                user_id=1,
                post_id=1,
                vote_type=VotesType.plus,
            )
            votes_minus = Vote(
                user_id=1,
                post_id=2,
                vote_type=VotesType.minus,
            )
            session.add_all([votes_plus, votes_minus])
            await session.commit()


class CreateDB:
    @staticmethod
    async def create_tables():
        async with async_engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
            await conn.run_sync(Base.metadata.create_all)


class PostService:
    @staticmethod
    async def create_post(theme: str, content: str, author: int):
        async with async_session_factory() as session:
            new_post = Post(theme=theme, content=content, author=author)
            session.add(new_post)
            await session.commit()
            await session.refresh(new_post)
            result_dto = PostsDTO.model_validate(new_post, from_attributes=True)
            return result_dto

    @staticmethod
    async def get_post_by_types(types: str):
        if types == 'last':
            order_by_field = Post.created_at
        elif types == 'rating':
            order_by_field = Post.rating
        else:
            order_by_field = None

        query = select(Post).order_by(desc(order_by_field)) if order_by_field else select(Post)

        async with async_session_factory() as session:
            result = await session.execute(query)
            posts = result.scalars().all()
            result_dto = [PostsDTO.model_validate(row, from_attributes=True) for row in posts]
            return result_dto

    @staticmethod
    async def check_post(post_id: int):
        async with async_session_factory() as session:
            query = select(Post).where(Post.id == post_id)
            result = await session.execute(query)
            post = result.scalars().one_or_none()
            return post


class UserService:
    @staticmethod
    async def get_user_by_username(username: str):
        async with async_session_factory() as session:
            query = select(User).where(User.username == username)
            result = await session.execute(query)
            user = result.scalars().one_or_none()
            if user:
                result_dto = UserSchema.model_validate(user, from_attributes=True)
                return result_dto
            return None


class VoteService:

    @staticmethod
    async def change_rating(post_id: int):
        async with async_session_factory() as session:
            query = union_all(
                select(func.count(Vote.id)).where(Vote.post_id == post_id,
                                                  Vote.vote_type == VotesType.minus),
                select(func.count(Vote.id)).where(Vote.post_id == post_id, Vote.vote_type == VotesType.plus)
            )

            result = await session.execute(query)
            counts = result.scalars().fetchall()

            count_minus = counts[0]
            count_plus = counts[1]

            difference = count_plus - count_minus

            post_query = select(Post).where(Post.id == post_id)
            result = await session.execute(post_query)
            res = result.scalar()
            res.rating = difference
            await session.commit()

    @staticmethod
    async def give_votes(post_id: int, user_id: int, vote_type: VotesType):
        async with async_session_factory() as session:
            existing_vote = select(Vote).filter(
                Vote.post_id == post_id,
                Vote.user_id == user_id
            )
            result = await session.execute(existing_vote)
            res = result.scalars().one_or_none()

            if res and res.vote_type == vote_type:
                return {"msg": "You can't vote again"}

            if res:
                await session.delete(res)

            if vote_type != VotesType.cancel:
                new_vote = Vote(post_id=post_id, user_id=user_id, vote_type=vote_type)
                session.add(new_vote)

            await session.commit()

            await VoteService.change_rating(post_id=post_id)

            return {
                "msg": f"Vote {'canceled' if vote_type == VotesType.cancel else 'added on ' + vote_type.name.lower()}"}

