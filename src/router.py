from fastapi import APIRouter, Depends, HTTPException, status, Path
from orm import CreateDB, PostService, UserService, VoteService
from schemas import PostsAddDTO, PostsDTO, VoteCreate, VotesType
from typing import List
from auth.jwt_auth import get_current_active_auth_user, get_current_token_payload


router = APIRouter(prefix='/api', tags=['posts'], dependencies=[Depends(get_current_active_auth_user)])


@router.post('/create-new-post', response_model=PostsDTO)
async def create_new_post(post_data: PostsAddDTO, payload: dict = Depends(get_current_token_payload)):
    theme = post_data.theme
    content = post_data.content
    author = payload.get('username')
    res = await PostService.create_post(theme=theme, content=content, author=author)
    return res


@router.get('/get-posts/{types}', response_model=List[PostsDTO])
async def get_sorted_posts_by_type(
    types: str = Path(..., description="Тип постов для сортировки ('last', 'rating' or other)")
):
    res = await PostService.get_post_by_types(types=types)
    return res


@router.post("/publications/{post_id}/vote")
async def vote_for_post(
        post_id: int,
        vote_type: VoteCreate,
        payload: dict = Depends(get_current_token_payload),
):
    user_id = payload.get('sub')
    if not await PostService.check_post(post_id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='post not found')

    res = await VoteService.give_votes(vote_type=vote_type.vote_type, user_id=user_id, post_id=post_id)
    return res


