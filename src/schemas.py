from pydantic import BaseModel, EmailStr
from datetime import datetime
from models import VotesType


class PostsAddDTO(BaseModel):
    theme: str
    content: str


class PostsDTO(PostsAddDTO):
    id: int
    author: str
    created_at: datetime
    updated_at: datetime
    rating: int


class TokenInfo(BaseModel):
    access_token: str
    token_type: str


class UserSchema(BaseModel):
    id: int
    username: str
    password: bytes
    email: EmailStr | None = None
    active: bool = True


class VoteCreate(BaseModel):
    vote_type: VotesType
